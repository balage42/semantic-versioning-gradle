package hu.pmi.semanticversion

import org.gradle.api.*
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFiles
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.bundling.Zip
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.get
import org.intellij.markdown.flavours.commonmark.CommonMarkFlavourDescriptor
import org.intellij.markdown.html.HtmlGenerator
import org.intellij.markdown.parser.MarkdownParser
import java.io.File
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.StandardOpenOption
import java.time.LocalDateTime

private const val TASK_GROUP = "versioning"

open class SemanticVersioningPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        @Suppress("UnstableApiUsage")
        project.extensions.create<SemanticVersionExtension>("semanticVersion", project)

        with(project.tasks) {
            create("versionInfo", InfoTask::class.java) { group = TASK_GROUP }
            create("showReleaseNotes", ReleaseNotesTask::class.java) { group = TASK_GROUP }
            create("updateReleaseNotes", UpdateReleaseNotes::class.java) { group = TASK_GROUP }
            create("updateVersion", UpdateVersioning::class.java) { group = TASK_GROUP }
            create("release", ReleaseTask::class.java) { group = TASK_GROUP }
            create("closeVersion", CloseVersioningTask::class.java) { group = "releasing" }
        }
    }
}

open class SemanticVersioningTask : DefaultTask() {
    @Internal
    protected val ext = project.extensions["semanticVersion"] as SemanticVersionExtension

    @Internal
    protected val state: SemanticVersionState = ext.state
}

open class InfoTask : SemanticVersioningTask() {

    @Suppress("unused")
    @TaskAction
    fun action() {
        logger.lifecycle("Current branch: {} ({})", state.currentBranch, if (state.isBranchAccepted) "accepted" else "rejected")
        logger.lifecycle("Git state: {} -> {}", state.gitState, when (state.gitState) {
            GitState.VALID -> "releasing is possible"
            GitState.BEHIND -> "local branch is behind remote"
            GitState.AHEAD -> "local repo is dirty (there are uncommitted changes)"
            GitState.INVALID_BRANCH -> "creating release from current branch (${state.currentBranch}) is prohibited"
        })
        logger.lifecycle("Current version: {} ({})", state.currentVersion.releaseString, state.currentReleaseHash
                ?: "<no hash>")
        logger.lifecycle("Release stats:\n   incompabilities: {}\n   new features:    {}\n   bugfixes:        {}",
                state.releaseLogs[GitLogEntryTypes.INCOMPABILITIES]?.size,
                state.releaseLogs[GitLogEntryTypes.NEW_FEATURE]?.size,
                state.releaseLogs[GitLogEntryTypes.BUGFIXES]?.size)
        logger.lifecycle("New semantic version: {}", state.newVersion.releaseString)
    }
}


open class ReleaseNotesTask : SemanticVersioningTask() {
    @Suppress("unused")
    @TaskAction
    fun action() {
        fun list(title: String, list: List<String>) {
            logger.lifecycle("   {} ({}):", title, list.size)
            with(list) {
                if (isEmpty()) logger.lifecycle("      <none>")
                else logger.lifecycle(this.joinToString("\n      ", "      ", "\n"))
            }
        }

        logger.lifecycle("RELEASE NOTES:")
        list("Incompatible changes", state.releaseLogs.getValue(GitLogEntryTypes.INCOMPABILITIES))
        list("New features", state.releaseLogs.getValue(GitLogEntryTypes.NEW_FEATURE))
        list("Bugfixes", state.releaseLogs.getValue(GitLogEntryTypes.BUGFIXES))
        logger.lifecycle("CURRENT VERSION: ${state.currentVersion.versionSequence}")
        logger.lifecycle("    NEW VERSION: ${state.newVersion.versionSequence}")
    }
}


open class ReleaseTask : Zip() {
    private val state: SemanticVersionState = (project.extensions["semanticVersion"] as SemanticVersionExtension).state

    private fun addProjectResultToRelease(task: Task) {
        task.outputs.files
                .forEach {
                    if (it.extension == "zip") {
                        val srcZip = project.zipTree(it)
                        includeEmptyDirs = false
                        from(srcZip) {
                            eachFile {
                                this.path = (this.path.replaceBefore(delimiter = "/", replacement = ""))
                            }
                        }
                    } else {
                        from(it)
                    }
                }
    }

    override fun dependsOn(vararg paths: Any?): Task {
        super.dependsOn(*paths)

        paths.forEach { addProjectResultToRelease(project.tasks.getByPath(it.toString())) }

        return this
    }


    override fun copy() {
        super.copy()
        logger.lifecycle("Release ${state.newVersion.versionSequence} is created.")
    }


    init {
        outputs.upToDateWhen { false }
        from(project.file("version.txt"))
        destinationDirectory.set(project.file("_release"))
        archiveBaseName.set(project.name)

        project.gradle.taskGraph.whenReady {
            if (this.hasTask(this@ReleaseTask))
                this@ReleaseTask.archiveVersion.set(this@ReleaseTask.state.newVersion.releaseString)
        }


        project.afterEvaluate {
            val ext = project.extensions["semanticVersion"] as SemanticVersionExtension
            if (ext.releaseNotesFile != null) {
                this@ReleaseTask.dependsOn(":updateReleaseNotes")
            }
        }
    }
}

open class UpdateVersioning : SemanticVersioningTask() {

    init {
        dependsOn("updateReleaseNotes")
    }

    @TaskAction
    fun action() {
        logger.lifecycle("New version: ${state.newVersion.versionSequence}")
        project.version = state.newVersion.versionSequence
    }
}

open class CloseVersioningTask : SemanticVersioningTask() {


    @TaskAction
    fun action() {
        logger.info("C: ${state.currentVersion}   N: ${state.newVersion}   EQ: ${state.currentVersion == state.newVersion}")
        if (state.currentVersion == state.newVersion)
            logger.warn("No new version created. No need to publish.")
        else
            with(ext) {
                val newVersion = state.newVersion.releaseString
                logger.info("Publishing $newVersion to git.")
                state.runExternalCommand("git", "add", ".")
                state.runExternalCommand("git", "commit", "-m", "\"publishing version $newVersion\"")
                state.runExternalCommand("git", "tag", "-a", state.newVersionReleaseTag, "-m", "\"Version $newVersion is published.\"")
                state.runExternalCommand("git", "push", "origin", state.currentBranch, state.newVersionReleaseTag)
            }
    }

    init {
//        project.afterEvaluate {
//            this@CloseVersionTask.dependsOn(":release")
//        }
    }

}

open class UpdateReleaseNotes : SemanticVersioningTask() {

    @OutputFiles
    var releaseNoteFiles: MutableSet<File> = mutableSetOf()

    init {
        project.afterEvaluate {
            val ext = project.extensions["semanticVersion"] as SemanticVersionExtension
            if (ext.releaseNotesFile != null) {
                this@UpdateReleaseNotes.releaseNoteFiles.add(project.file(ext.releaseNotesFile!!))
                this@UpdateReleaseNotes.releaseNoteFiles.add(project.file(ext.releaseNotesHtmlFile!!))
            }
        }
    }

    @TaskAction
    fun action() {
        val ext = project.extensions["semanticVersion"] as SemanticVersionExtension

        if (ext.releaseNotesFile == null) {
            logger.lifecycle("No release log file specified in configuration")
            return
        } else {
            val f = project.file(ext.releaseNotesFile!!)
            releaseNoteFiles.add(f)

            val lines =
                    if (f.exists())
                        Files.readAllLines(f.toPath(), StandardCharsets.UTF_8)
                    else {
                        logger.lifecycle("No release log file was found in ${f.absolutePath}. Creating one from scratch.")
                        mutableListOf("# ${project.displayName}", "")
                    }

            var ip = 0
            while (ip < lines.size) {
                if (lines[ip].trimStart().startsWith("### ")) {
                    if (lines[ip].contains(state.newVersion.versionSequence) ||
                            (!ext.keepInternalReleases && lines[ip].contains(ext.internalReleaseLabel))) {
                        do {
                            lines.removeAt(ip)
                        } while (ip < lines.size && !lines[ip].trimStart().startsWith("### "))
                    }
                    break
                }
                ip++
            }

            val timestamp = ext.releaseTimeFormatter.format(LocalDateTime.now())
            val internal = if (state.isInternalRelease) "- ${ext.internalReleaseLabel}" else ""
            lines.add(ip++, "### Version ${state.newVersion.versionSequence} ($timestamp) $internal")
            lines.add(ip++, "")

            if (!state.isInternalRelease) {
                state.releaseLogs.forEach { (t, l) ->
                    if (l.isNotEmpty()) {
                        lines.add(ip++, "#### ${ext.logEntryLabels[t]}")
                        lines.add(ip++, "")
                        l.forEach {
                            lines.add(ip++, "- " + it.trim())
                        }
                    }
                }
                lines.add(ip++, "")
            }

            try {
                Files.write(f.toPath(), lines, StandardCharsets.UTF_8, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE)
            } catch (e: IOException) {
                throw GradleException("Error while updating release note file: ${e.message}", e)
            }

            val markupText = lines.joinToString("\n")
            val flavour = CommonMarkFlavourDescriptor()
            val parsedTree = MarkdownParser(flavour).buildMarkdownTreeFromString(markupText)
            val html = HtmlGenerator(markupText, parsedTree, flavour).generateHtml()
                    .replace("<body>", "<body><meta charset=\"utf-8\"/>")

            try {
                Files.write(
                        project.file(ext.releaseNotesHtmlFile!!).toPath(),
                        listOf(html),
                        StandardCharsets.UTF_8,
                        StandardOpenOption.CREATE,
                        StandardOpenOption.TRUNCATE_EXISTING
                )
            } catch (e: IOException) {
                throw GradleException("Error while generating change log HTML file: ${e.message}", e)
            }

            if (ext.versionFile != null) {
                try {
                    Files.write(
                            project.file(ext.versionFile!!).toPath(),
                            listOf(state.newVersion.releaseString),
                            StandardCharsets.UTF_8,
                            StandardOpenOption.CREATE,
                            StandardOpenOption.TRUNCATE_EXISTING
                    )
                } catch (e: IOException) {
                    throw GradleException("Error while updating version file: ${e.message}", e)
                }
            }

        }
    }

}

