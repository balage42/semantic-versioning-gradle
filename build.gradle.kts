import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
    jcenter()
    mavenCentral()
    maven("https://dl.bintray.com/jetbrains/markdown/")
}

buildscript {
    repositories {
        jcenter()
        mavenCentral()
        maven(url = "https://plugins.gradle.org/m2/")
    }
}

plugins {
    kotlin("jvm") version "1.3.41"
    `kotlin-dsl`
    java
    maven
//    id("signing")
    id("org.jetbrains.dokka") version "1.4.10.2"

    id("java-gradle-plugin")
    id("maven-publish")
    id("com.gradle.plugin-publish") version "0.12.0"
}


group = "hu.vissy.semantic-versioning"
version = "1.0.3"




dependencies {
    implementation(gradleApi())
    api("commons-net:commons-net:3.6")
    api("org.jetbrains:markdown:0.1.25")
}



pluginBundle {
    website = "https://bitbucket.org/balage42/semantic-versioning-gradle/src/master/"
    vcsUrl = "https://bitbucket.org/balage42/semantic-versioning-gradle/src/master/"
    tags = listOf("semantic-version", "deploy")
}



configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}


gradlePlugin {
    plugins {
        create("semantic-versioning") {
            id = "hu.vissy.semantic-versioning"
            displayName = "Semantic versioning through GIT support"
            description = "A semantic version support based on specially prefixed git commit messages."
            implementationClass = "hu.pmi.semanticversion.SemanticVersioningPlugin"
        }
    }
}


tasks {

    withType<Test> { useTestNG() }

    withType<JavaCompile> {
        options.encoding = "UTF-8"
        sourceCompatibility = "11"
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }

    val sourcesJar by creating(Jar::class) {
        archiveClassifier.set("sources")
        from(sourceSets.main.get().allSource)
    }

    val javadocJar by creating(Jar::class) {
        dependsOn.add(dokkaJavadoc)
        archiveClassifier.set("javadoc")
        from(dokkaJavadoc)
    }

    artifacts {
        archives(sourcesJar)
        archives(javadocJar)
        archives(jar)
    }
}